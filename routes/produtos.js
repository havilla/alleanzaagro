module.exports = function(app){
	var produtos = app.controllers.produtos;
	app.route('/produtos/lotus')
		.get(produtos.lotus);
	app.route('/produtos/vita')
		.get(produtos.vita);
	app.route('/produtos/dskfull')
		.get(produtos.dskfull);
	app.route('/produtos/secta')
		.get(produtos.secta);
	app.route('/produtos/organicf')
		.get(produtos.organicf);
	app.route('/produtos/d')
		.get(produtos.d);
	app.route('/produtos/cgaplus')
		.get(produtos.cgaplus);
	app.route('/produtos/vpower')
		.get(produtos.vpower);
}