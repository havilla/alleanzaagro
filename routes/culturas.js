module.exports = function(app){
	var culturas = app.controllers.culturas;
	app.route('/cultura/citros')
		.get(culturas.citros);
	app.route('/cultura/arroz')
		.get(culturas.arroz);
	app.route('/cultura/algodao')
		.get(culturas.algodao);
	app.route('/cultura/batata')
		.get(culturas.batata);
	app.route('/cultura/trigo')
		.get(culturas.trigo);
	app.route('/cultura/milho')
		.get(culturas.milho);
	app.route('/cultura/cana')
		.get(culturas.cana);
	app.route('/cultura/soja')
		.get(culturas.soja);
	app.route('/cultura/tomate')
		.get(culturas.tomate);
	app.route('/cultura/cafe')
		.get(culturas.cafe);
	app.route('/cultura/pastagem')
		.get(culturas.pastagem);
	app.route('/cultura/feijao')
		.get(culturas.feijao);
}