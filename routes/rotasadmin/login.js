module.exports = function(app) {
	var login = app.controllers.painel.login;
	var autenticar = require('../../middleware/autenticar');
	app.route('/login')
		.get(login.login)
		.post(login.autenticacao);
	app.route('/painel/noticias')
		.get(autenticar, login.noticias)
		.post(login.salvarNoticia)
	app.route('/painel/noticias/:numero')
		.get(autenticar, login.noticia_buscar)
	app.route('/logout')
		.get(login.logout);
}