module.exports = function(app) {
	var Parceiros = app.controllers.painel.parceiros;
	var autenticar = require('../../middleware/autenticar');
	app.route('/painel/parceiros')
        .get(autenticar, Parceiros.index)
        .post(Parceiros.buscar);
    app.route('/painel/parceiros/create')
        .get(autenticar, Parceiros.create)
    .post(autenticar, Parceiros.salvar);
    app.route('/painel/parceiros/edit/:id')
        .get(autenticar, Parceiros.editar)
        .post(Parceiros.update);
        // .post(autenticar, Parceiros.salvar);
}