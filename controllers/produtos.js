module.exports = function(app) {
	var HomeController = {
		lotus: function(req, res) {
			res.render('produtos/lotus');
		},
		vita: function(req, res) {
			res.render('produtos/vita');
		},
		dskfull: function(req, res) {
			res.render('produtos/dskfull');
		},
		secta: function(req, res) {
			res.render('produtos/secta');
		},
		organicf: function(req, res) {
			res.render('produtos/organicf');
		},
		d: function(req, res) {
			res.render('produtos/d');
		},
		cgaplus: function(req, res) {
			res.render('produtos/cgaplus');
		},
		vpower: function(req, res) {
			res.render('produtos/vpower');
		}
	}
	return HomeController;
}