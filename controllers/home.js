module.exports = function(app) {
	var HomeController = {
		index: function(req, res) {
			res.render('home');
		},
		parceiros: function(req, res) {
			res.render('parceiros');
		}
	}
	return HomeController;
}