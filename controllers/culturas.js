module.exports = function(app) {
	var HomeController = {
		citros: function(req, res) {
			res.render('culturas/citros');
		},
		arroz: function(req, res) {
			res.render('culturas/arroz');
		},
		algodao: function(req, res) {
			res.render('culturas/algodao');
		},
		batata: function(req, res) {
			res.render('culturas/batata');
		},
		trigo: function(req, res) {
			res.render('culturas/trigo');
		},
		milho: function(req, res) {
			res.render('culturas/milho');
		},
		cana: function(req, res) {
			res.render('culturas/cana');
		},
		soja: function(req, res) {
			res.render('culturas/soja');
		},
		tomate: function(req, res) {
			res.render('culturas/tomate');
		},
		cafe: function(req, res) {
			res.render('culturas/cafe');
		},
		pastagem: function(req, res) {
			res.render('culturas/pastagem');
		},
		feijao: function(req, res) {
			res.render('culturas/feijao');
		}
	}
	return HomeController;
}