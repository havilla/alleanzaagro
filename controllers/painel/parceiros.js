module.exports     = function(app) {
const formidable = require('formidable');
const fs = require('fs');

const Parceiros          = app.models.parceiros;
const autenticacao   = require('../../validacoes/autenticacao');
const HomeController = {
        index: (req,res)=>{
          Parceiros.find((err,dados)=>{
            if(err){
              req.flash('erro', 'Erro ao carregar: '+ err);
              res.render('admin/parceiros/index', {lista: null});
            }
              res.render('admin/parceiros/index', {lista: dados});
          })
        },
        create: (req,res)=>{
          res.render('admin/parceiros/create', {model: new Parceiros})
        },
        salvar: (req, res) => {
          var form = new formidable.IncomingForm();
          form.parse(req, function(err, fields, files) {
            // res.writeHead(200, {
            //   'content-type': 'text/plain'
            // });
            // res.write('received upload:\n\n');
            var image = files.image,
              // image_upload_path_old = replaceSpecialChars(image.path),
              image_upload_path_old = image.path,
              image_upload_path_new = './public/images/parceiros/',
              // image_upload_name = image.name,
              image_upload_name = (fields.nome.replace( /\s/g, '' ))+fields.uf+fields.cidade,
              image_upload_path_name = image_upload_path_new + image_upload_name;
            if (fs.existsSync(image_upload_path_new)) {
              fs.rename(
                image_upload_path_old,
                image_upload_path_name,
                function(err) {

                  if (err) {
                    console.log('Err: ', err);
                    res.end('Erro na hora de mover a imagem!');
                  }

                  var msg        = 'Imagem ' + image_upload_name + ' salva em: ' + image_upload_path_new;
                  var model      = new Parceiros();
                  model.nome     = fields.nome.toUpperCase();
                  model.telefone = fields.telefone;
                  model.image    = image_upload_name;
                  model.uf       = fields.uf.toUpperCase();
                  model.cidade   = fields.cidade.toUpperCase();

                  console.log("passou pelas variaveis");
                      model.save(function(err) {
                        if (err) {
                          req.flash('erro', 'Erro ao cadastrar: ' + err);
                          res.render('admin/noticias', {
                            user: fields
                          });
                                            // res.end(msg);
                        } else {
                          req.flash('info', 'Registro cadastrado com sucesso!');
                          // res.redirect('/admin/noticias');
                          // res.end();
                          // res.render('admin/noticias');
                        }
                      });
                      console.log("passou pelo model");

                });
            } else {
              fs.mkdir(image_upload_path_new, function(err) {
                if (err) {
                  console.log('Err: ', err);
                  res.end('Erro na hora de criar o diretório!');
                }
                fs.rename(
                  image_upload_path_old,
                  image_upload_path_name,
                  function(err) {
                    var msg = 'Imagem ' + image_upload_name + ' salva em: ' + image_upload_path_new;
                    console.log(msg);
                    res.end(msg);
                  });
              });
            }

          });
          res.redirect('/painel/parceiros');
        },
        editar: (req, res) => {
            Parceiros.findById(req.params.id, function(err, data) {
                if (err) {
                    req.flash('erro', 'Erro ao carregar. ' + err);
                    res.redirect('/painel/parceiros');

                } else {
                    res.render('admin/parceiros/edit', {
                        model: data
                    });
                }
            })
        },
        update2: (req, res) => {
            Parceiros.findById(req.params.id, function(err, data) {
              console.log(data)
              console.lot(filds)
              var model      = data;
              model.nome     = fields.nome.toUpperCase();
              model.telefone = fields.telefone;
              model.image    = image_upload_name;
              model.uf       = fields.uf.toUpperCase();
              model.cidade   = fields.cidade.toUpperCase();

              model.save(function(err) {
                if (err) {
                  req.flash('erro', 'Erro ao cadastrar: ' + err);
                  res.render('admin/noticias', {
                    user: fields
                  });
                  // res.end(msg);
                } else {
                  req.flash('info', 'Registro cadastrado com sucesso!');
                  // res.redirect('/admin/noticias');
                  // res.end();
                  // res.render('admin/noticias');
                }
              });

            })
        },
        update: (req, res) => {
          var form = new formidable.IncomingForm();
          form.parse(req, function(err, fields, files) {
            // console.log('$$$fields$$$');
            // console.log(fields);
            console.log('___________________--files--___________________________-');
            console.log(files.image.name == '');

            var image = files.image,
              image_upload_path_old = image.path,
              image_upload_path_new = './public/images/parceiros/',
              image_upload_name = (fields.nome.replace( /\s/g, '' ))+fields.uf+fields.cidade,
              image_upload_path_name = image_upload_path_new + image_upload_name;

            if (files.image.name != '') {
              fs.rename(
                image_upload_path_old,
                image_upload_path_name,
                function(err) {

                  if (err) {
                    console.log('Err: ', err);
                    res.end('Erro na hora de mover a imagem!');
                  }

                  var msg        = 'Imagem ' + image_upload_name + ' salva em: ' + image_upload_path_new;

                  // console.log("passou pelas variaveis");
                  // console.log(req.params.id);
                  Parceiros.findById(req.params.id, function(err, data) {
                  var model      = data;
                  model.nome     = fields.nome.toUpperCase();
                  model.telefone = fields.telefone;
                  model.image    = image_upload_name;
                  model.uf       = fields.uf.toUpperCase();
                  model.cidade   = fields.cidade.toUpperCase();
                    // console.log(data)
                    // console.log(fields)

                    model.save(function(err) {
                      if (err) {
                        req.flash('erro', 'Erro ao cadastrar: ' + err);
                        res.render('admin/noticias', {
                          user: fields
                        });
                      } else {
                        req.flash('info', 'Registro cadastrado com sucesso!');

                      }
                    });

                  })
                      console.log("passou pelo model");

                });
            } else {
                  Parceiros.findById(req.params.id, function(err, data) {
                  var model      = data;
                  model.nome     = fields.nome.toUpperCase();
                  model.telefone = fields.telefone;
                  model.uf       = fields.uf.toUpperCase();
                  model.cidade   = fields.cidade.toUpperCase();
                    // console.log(data)
                    // console.log(fields)

                    model.save(function(err) {
                      if (err) {
                        req.flash('erro', 'Erro ao cadastrar: ' + err);
                        res.render('admin/noticias', {
                          user: fields
                        });
                      } else {
                        req.flash('info', 'Registro cadastrado com sucesso!');

                      }
                    });

                  })
                      console.log("passou pelo model");
            }

          });
          res.redirect('/painel/parceiros');
        },
        buscar: (req, res) => {
          console.log('________________chegou no bucar______________')

          var param = req.body;
          console.log(req.body)
          console.log(req.body.uf)
          // console.log(JSON.parse(param))
          // console.log(JSON.stringify(param))
          console.log('______________________________________________')

          var pesquisa = {};

        if (param.nome){
          pesquisa.nome = {$regex : '.*'+param.nome.toUpperCase()+'.*'}
        }

        if (param.uf){
          pesquisa.uf = {$regex : '.*'+param.uf.toUpperCase()+'.*'}
        } 

        if (param.telefone){
          pesquisa.telefone = {$regex : '.*'+param.telefone+'.*'}
        } 
          // console.log('__________________________');
          // console.log("pesquisa")
          // console.log(pesquisa)
          Parceiros.find(pesquisa)
                  .select('nome')
                  .select('uf')
                  .select('telefone')
                  .select('data_cad')
                  .sort({data_cad: -1})
                  .exec(function(err,dados) {
                    if (err) {
                      console.log("Entrou no erro da busca")
                      req.flash('erro', 'Erro ao buscar noticia' + err);
                      // res.redirect('/orcamento');
                    } else {
                      console.log('dados')
                      // console.log(dados)
                      // Noticias.find({estado:true})
                      //   .select('estado')
                      //   .exec(function(err, Dados){
                      //     console.log('Dados.length')
                      //     console.log(Dados.length)
                      
                      // res.render('admin/parceiros/index', {lista: dados});
                      //   })
                      // console.log(dados.length)
                      res.json({lista: dados});
                    }
                })
        },
	}
function replaceSpecialChars(str)
{
    str = str.replace(/[ÀÁÂÃÄÅ]/,"A");
    str = str.replace(/[àáâãäå]/,"a");
    str = str.replace(/[ÈÉÊË]/,"E");
    str = str.replace(/[Ç]/,"C");
    str = str.replace(/[ç]/,"c");

    // o resto

    return str.replace(/[^a-z0-9]/gi,''); 
}

	return HomeController;
}