module.exports     = function(app) {
var Admin          = app.models.admin;
var Noticias          = app.models.noticias;
var autenticacao   = require('../../validacoes/autenticacao');
var HomeController = {
		login: function(req, res){
			res.render('admin/login');
		},
		autenticacao: function(req, res) {
			var admin = new Admin();
			var email = req.body.email;
			var password = req.body.password;

		if (autenticacao(req, res)) {
			Admin.findOne({'email': email}, function(err, data) {
					if (err) {
						req.flash('erro', 'Erro ao entrar no sistema: ' + err);
						res.redirect('/login');
					} else if (!data) {
						req.flash('erro', 'E-mail não encontrado');
						res.redirect('/login');

					} else if (!admin.validPassword(password, data.password)) {
						req.flash('erro', 'Senha não confere!');
						res.redirect('/login');
					} else {
						req.session.admin = data;
						res.redirect('/painel/noticias');

					}
				});

			}else{
				res.redirect('/');

			}
		},
		noticias: function(req, res){
            console.log('chegou aqui2');
			res.render('admin/noticias');
		},
		salvarNoticia: function(req, res){
			console.log("req.body:")
			console.log(req.body._id)
			var noticia = new Noticias();
            if (req.body._id) {
                // console.log("possui id")
                // console.log(noticia)
                // noticia._id = req.body._id;
                Noticias.findById(req.body._id, function(err, dados) {
                    var model = dados;
                    model.estado = req.body.estado;
                    model.linhas = req.body.linhas;
                    model.save(function(err, dados) {
                        if (dados) {
                            res.send(200)
                                // req.flash('erro', 'Erro ao cadastrar '+err);
                                // res.render('amigos/edit',{model: model});
                        } else {
                            // res.flash('info', 'Registro atualizado com sucesso!');
                            // res.render('/amigos');
                        }

                    })
                })

            } else {
                // var noticia = new Noticias();
                // console.log(noticia);
                Noticias.count(function(err, count) {
                    // console.log("count")
                    // console.log(count)
                    noticia.estado = req.body.estado;
                	noticia.linhas = req.body.linhas;
                    noticia.numero = count;
                    noticia.save(function(err) {
                        if (err) {
                            // req.flash('erro', 'Erro ao cadastrar: ' + err);
                            // res.render('usuarios/create', {
                            // 	user: req.body
                            // });

                            console.log("não salvou")
                        } else {
                            // req.flash('info', 'Registro cadastrado com sucesso!');
                            // res.redirect('/usuarios');
                            res.send(200)
                            console.log("salvou")

                        }
                    });
                })

            }			
		},
        noticia_buscar: function(req, res){
                var model   = new Noticias();
                model.numero    = req.params.numero;
                console.log("req.body")
                // console.log(req.params.numero)
                Noticias.findOne({'numero': model.numero}, function(err,dados){
                    if (dados) {
                        // req.flash('erro', 'Nome encontra-se cadastrado, informe outro.');
                        // res.render('amigos/create',{model: model});
                        // console.log("cadastro localizado")
                        // console.log(dados)
                        res.send(dados)
                    }else{
                        // console.log("cadastro não encontrado")
                        res.send(800);
                        // model.save(function(err) {
                        //  if (err) {
                        //      req.flash('erro', 'Erro ao cadastrar: ' + err);
                        //      res.render('usuarios/create', {
                        //          user: req.body
                        //      });
                        //  } else {
                        //      req.flash('info', 'Registro cadastrado com sucesso!');
                        //      res.redirect('/usuarios');
                        //  }
                        // });
                        // res.flash('info', 'Registro cadastrado com sucesso!');
                        // res.render('/amigos');
                    }
                })
        },
		logout: function(req,res){
			req.session.destroy();
			res.redirect('/login');
		}
	}
	return HomeController;
}