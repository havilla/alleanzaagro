module.exports = function(app) {
	var Noticias = app.models.noticias;
	var HomeController = {
		// index: function(req, res) {
		// 	res.render('noticias');
		// },
		noticias: function(req, res) {
			// res.render('noticia/index');
			var inicio = 0;
				var quantidade = 5;
				console.log('req.params')
				console.log(req.params.id)
				if (parseInt(req.params.id)>1){
					inicio = (parseInt(req.params.id)-1)*5;
					quantidade = 5*parseInt(req.params.id);
				}
			Noticias.find({estado:true})
				.sort({data_cad: -1})
				.skip(inicio)
				.limit(5)
				.exec(function(err,dados) {
					if (err) {
						req.flash('erro', 'Erro ao buscar noticia' + err);
						res.redirect('/orcamento');
					} else {
						Noticias.find({estado:true})
							.select('estado')
							.exec(function(err, Dados){
								console.log('Dados.length')
								console.log(Dados.length)
								res.render('noticias', {Data: dados, dadosNumero: dados[(dados.length-(dados.length-1))-1].numero, qnt: Dados.length, id: req.params.id});
							})
						// console.log(dados.length)
						// res.json({Data: dados,Contador: count, success: true, message: 'Busca ok'});
					}
			})
		},
		noticia_xl: function(req, res) {
			console.log(req.params.id)
			// res.render('noticia/xl');
			Noticias.findOne({'numero': req.params.id}, function(err, data) {
			// Noticias.find()
					if (err) {
						req.flash('erro', 'Erro ao buscar noticia' + err);
						res.redirect('/orcamento');
					} else {
						console.log(data.data_cad)
						// res.json({Data: dados,Contador: count, success: true, message: 'Busca ok'});
						res.render('noticiasxl', {Data: data.linhas, DataReal: formatarData(data.data_cad)});
					}
			})
		}
	}
	return HomeController;
	//Formatar data, exemplo: Quarta-feira, 26 de Janeiro de 2018.
	function formatarData(data){
		var dataFormatada;
		var dia = ["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"][data.getDay()];
		var date = data.getDate();
		var mes = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"][data.getMonth()];
		var ano = data.getFullYear();
		dataFormatada=dia+", "+date+" de "+mes+" de "+ano;
  		return dataFormatada;
	}
}