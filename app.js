const express          = require('express');
const path             = require('path');
const favicon          = require('static-favicon');
const logger           = require('morgan');
const cookieParser     = require('cookie-parser');
const bodyParser       = require('body-parser');
const session          = require('express-session');
const load             = require('express-load');
const mongoose         = require('mongoose');
const flash            = require('express-flash');
const moment           = require('moment');
const expressValidator = require('express-validator');
const formidable       = require('formidable');


// CONEXÃO COM O BANCO
mongoose.connect('mongodb://localhost/alleanza', function(err){
  if(err){
    console.log("Erro ao conectar no mongodb "+err);
  }else{
    console.log("Conexão com mongodb efetuada com sucesso!");
  }
});

var app = express();

//middleware
var erros = require('./middleware/erros');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(expressValidator());
app.use(cookieParser());
app.use(session({ secret: 'secreto123' }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(flash());

// helpers
app.use(function(req,res,next){
  res.locals.session = req.session.admin;
  // res.locals.sessionAdmin = req.session.loginAdmin;
  res.locals.isLogged = req.session.admin ? true : false;
  // res.locals.isLoggedAdmin = req.session.loginAdmin ? true : false;
  res.locals.moment = moment;
  next();
});

load('models').then('controllers').then('routes').into(app);

//middleware
// app.use(erros.notfound);
// app.use(erros.serverError);
app.listen(3000, function() {
    console.log('Express server listening on port 3000');
});
