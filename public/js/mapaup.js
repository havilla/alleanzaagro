$(function() {
	var map_settings = {
		map: 'brazil',
		zoomButtons: false,
		zoomMax: 1,
		normalizeFunction: 'polynomial',
		regionStyle: {
			initial: {
				'fill-opacity': 1,
				stroke: '#000',
				'stroke-width': 100,
				'stroke-opacity': 1
			},
			hover: {
				fill: '#3e774e'
			}
		},
		markerStyle: {
		initial: {
		fill: '#F8E23B',
		stroke: '#383f47'
		}
		},
		backgroundColor: '#fdfdfd00',
		series: {
			regions: [{
				values: {
					// Região Norte
					ac: '#29689A',
					am: '#29689A',
					ap: '#29689A',
					pa: '#29689A',
					ro: '#29689A',
					rr: '#29689A',
					to: '#29689A',
					// Região Nordeste
					al: '#29689A',
					ba: '#29689A',
					ce: '#29689A',
					ma: '#29689A',
					pb: '#29689A',
					pe: '#29689A',
					pi: '#29689A',
					rn: '#29689A',
					se: '#29689A',
					// Região Centro-Oeste
					df: '#29689A',
					go: '#29689A',
					ms: '#29689A',
					mt: '#29689A',
					// Região Sudeste
					es: '#29689A',
					mg: '#29689A',
					rj: '#29689A',
					sp: '#29689A',
					// Região Sul
					pr: '#29689A',
					rs: '#29689A',
					sc: '#29689A'
				},
				attribute: 'fill'
			}]
		},
		container: $('#brazil-map'),
		onRegionClick: function(event, code) {
			// $('#clicked-region span').text(code);
			buscarParceiro(code);
		},
		onRegionOver: function(event, code) {
			// console.log(event)
			// $('#hovered-region span').text(code);
			// myFunction(code,event)
		},
		onRegionOut: function(event, code) {
			// clearCoor()
			// myFunction(code,event)
		},

	};
	map = new jvm.WorldMap($.extend(true, {}, map_settings));
});

function buscarParceiro(code){
    // event.preventDefault();
    var param = {};
        param.uf = code.toUpperCase();
    fetch('/painel/parceiros',{
        method:'POST',
        headers:{'Content-Type':'application/json'},
        body: JSON.stringify(param)
    })
    .then(function(response){
        return response.json();
    })
    .then(function(response){
	    document.getElementById('tbody').innerHTML = ''
	    if(response.lista == ""){
			ParceirosNot()
	    }else{
	        atualizaParceiros(response.lista)
	        atualizaParceiros(response.lista)
	        atualizaParceiros(response.lista)
	        atualizaParceiros(response.lista)
	        atualizaParceiros(response.lista)
	        atualizaParceiros(response.lista)
	        atualizaParceiros(response.lista)
	        atualizaParceiros(response.lista)
	        atualizaParceiros(response.lista)
	        atualizaParceiros(response.lista)
	    }
    })   
}
//gera varias linhas dentro do tbody
function atualizaParceiros(lista) {
	// console.log("lista:");
	// console.log(lista);
    for (var i = 0; i < lista.length; i++) {
        var table = document.getElementById('tbody');
        var row = table.insertRow(0);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        cell1.innerHTML = lista[i].nome;
        cell2.innerHTML = lista[i].uf;
        cell3.innerHTML = lista[i].telefone;
    }

}
function ParceirosNot() {
	var table = document.getElementById('tbody');
	var row = table.insertRow(0);
	var cell1 = row.insertCell(0);
	cell1.innerHTML = 'Nenhum parceiro foi localizado para este estado.';

}


// function clearCoor() {
// 	var position = document.getElementById("position");
// 	position.style.opacity = 0;
// 	document.getElementById("position").innerHTML = "";
// }