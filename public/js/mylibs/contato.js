function dadosEmailContato(){
	var dados = {};
	dados.nome = $("#form-contato .nome").val()
	dados.email = $("#form-contato .email").val()
	dados.telefone = $("#form-contato .telefone").val()
	dados.cidade = $("#form-contato .cidade").val()
	dados.estado = $("#form-contato .estado").val()
	dados.assunto = $("#form-contato .assunto").val()
	dados.mensagem = $("#form-contato .mensagem").val()
	// console.log(dados)
	enviarEmailContato(JSON.stringify(dados));
	document.querySelector("#form-contato").reset();
}
function enviarEmailContato(param){
	$.ajax({
		type: 'POST',
		contentType: "application/json",
		url: "/contato/email",
		data: param,
		beforeSend: function(){
			loading();
		},
		success: function(data, textStatus, jqXHR, Exception){
			alertGlobal("Email Enviado Com Sucesso!","FECHAR");
			$("#loading").remove();
		},
		error: function(jqXHR, textStatus){
			alertGlobal("Tente novamente!","" , "","FECHAR");
			$("#loading").remove();
		}
	});
}
$(function(){
	$(".botaoEnviar").click(function(event) {
		event.preventDefault();
		if(ValidarDadosContato()){
			dadosEmailContato();
		}
	});
})