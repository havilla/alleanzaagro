function validacaoEmail(field) {
	usuario = field.value.substring(0, field.value.indexOf("@"));
	dominio = field.value.substring(field.value.indexOf("@") + 1, field.value.length);

	if ((usuario.length >= 1) &&
		(dominio.length >= 3) &&
		(usuario.search("@") == -1) &&
		(dominio.search("@") == -1) &&
		(usuario.search(" ") == -1) &&
		(dominio.search(" ") == -1) &&
		(dominio.search(".") != -1) &&
		(dominio.indexOf(".") >= 1) &&
		(dominio.lastIndexOf(".") < dominio.length - 1)) {
		// document.getElementById("msgemail").innerHTML = "E-mail válido";
		return true;
	} else {
		// document.getElementById("msgemail").innerHTML = "<font color='red'>E-mail inválido </font>";
		// alertGlobal("Email Invalido!","FECHAR");
		return false;
	}
}

function ValidarDadosContato() {
	if ($('.nome').val() == "") {
		alertGlobal("Campo Nome Vazio!");
	} else if ($('#form-contato .email').val() == "") {
		alertGlobal("Campo E-mail Vazio!");
	}else if (!validacaoEmail(document.querySelector('#form-contato .email'))) {
		alertGlobal("Campo E-mail Invalido!");
	}else if ($('#form-contato .telefone').val() == 0) {
		alertGlobal("Campo Telefone Vazio!");
	}else if ($('#form-contato .assunto').val() == "") {
		alertGlobal("Campo Assunto Vazio!");
	}else if ($('#form-contato .mensagem').val() == "") {
		alertGlobal("Campo Mensagem Vazio!");
	}else{
		return true
	}
}
var focusAlert;
// function alertGlobal(img, msg, id, nomeBt){
// function alertGlobal(msg, id, img){
function alertGlobal(msg, focus, img, nom1btn, nom2btn, opc, parm) {
	focusAlert = focus;
	if (opc == undefined) {
		console.log(opc)
		$("body").append(
			'<section class="wrap">' +
				'<aside class="corpo">' +
					'<aside class="before">' +
						'<span class="imgBc ' + img + '"></span>' +
						'<span>' + msg + '</span>' +
						'<button class="btn_Alert">FECHAR</button>' +
					'</aside>' +
				'</aside>' +
			'</section>');
		$('.btn_Alert').bind('click', excluirAlertGolbal);
	} else {
		$("body").append(
			'<section class="wrap">' +
				'<aside class="corpo">' +
					'<aside class="before">' +
						'<span class="imgBc ' + img + '"></span>' +
						'<span>' + msg + '</span>' +
						'<button class="fecharSim">' + nom1btn + '</button>' +
						'<button class="fecharNao">' + nom2btn + '</button>' +
					'</aside>' +
				'</aside>' +
			'</section>');
	}
	$(".fecharSim, .fecharNao").bind('click', excluirAlertGolbal);
}
function excluirAlertGolbal() {
	var rem = $(this).parent().parent().parent();
	rem.remove();
	// $("#" + focusAlert).focus();
}
function loading(){
	$('body').append(
		'<section id="loading">'+
			'<div class="divLoading">'+
				'<img class="imgLoading" src="../../../images/bx_loader.gif">'+
			'</div>'+
		'</section>'
	)
	$("#loading").addClass('loadingColor');
	return true;
}
$('#navbar li a, section, #btnOrcamento2').click(function(event) {
	$('#botaoMenu').addClass('collapsed')
	$('#navbar').removeClass('in')
	$('#botaoMenu').attr("aria-expanded", false)
});