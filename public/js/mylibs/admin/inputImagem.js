function previewFile() {
  
  console.log(this);
  console.log(this.files[0]);
  var preview = document.querySelector("#imagem-"+this.id);
  var file    = this.files[0];
  var reader  = new FileReader();
  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }

}