// formBuscar.addEventListener("submit",buscarParceiro());

formBuscar.nome.oninput = pegaTecla;
formBuscar.uf.oninput = pegaTecla;
formBuscar.telefone.oninput = pegaTecla;

function pegaTecla(){
    this.value = this.value.toUpperCase()
    if(this.value.length>=2){
        buscarParceiro()

    }
    if(this.value==""){
        buscarParceiro()
    }

}
function buscarParceiro(){
    // event.preventDefault();
    console.log("adcionou evento")
    var param = {};
        param.nome = formBuscar.nome.value
        param.uf = formBuscar.uf.value
        param.telefone = formBuscar.telefone.value
    fetch('/painel/parceiros',{
        method:'POST',
        headers:{'Content-Type':'application/json'},
        body: JSON.stringify(param)
    })
    .then(function(response){
        return response.json();
    })
    .then(function(response){
    document.getElementById('tbody').innerHTML = ''
        atualizaParceiros(response.lista)
    })   
}
//gera varias linhas dentro do tbody
function atualizaParceiros(lista) {
    for (var i = 0; i < lista.length; i++) {
        var table = document.getElementById('tbody');
        var row = table.insertRow(0);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        cell1.innerHTML = lista[i].nome;
        cell2.innerHTML = lista[i].uf;
        cell3.innerHTML = lista[i].telefone;
        cell4.innerHTML = dataFormatada(lista[i].data_cad);
        cell5.innerHTML = ''+
            '<a class="btn btn-info" href="/painel/parceiros/show/'+lista[i]._id+'">'+
                '<svg class="icon icon-search"><use xlink:href="#icon-search"></use></svg></a>'+
            '<a class="btn btn-primary" href="/painel/parceiros/edit/'+lista[i]._id+'">'+
                '<svg class="icon icon-pencil"><use xlink:href="#icon-pencil"></use></svg></a>'+
            '<a class="btn btn-success" href="/painel/parceiros/'+lista[i]._id+'">'+
                '<svg class="icon icon-plus"><use xlink:href="#icon-plus"></use></svg></a>';
    }

}

function dataFormatada(d){
    var data = new Date(d),
        dia  = data.getDate().toString(),
        diaF = (dia.length == 1) ? '0'+dia : dia,
        mes  = (data.getMonth()+1).toString(), //+1 pois no getMonth Janeiro começa com zero.
        mesF = (mes.length == 1) ? '0'+mes : mes,
        anoF = data.getFullYear();
    return diaF+"/"+mesF+"/"+anoF;
}