// document.querySelector(".inputfile").addEventListener("change", previewFile);
// document.querySelector(".inputfile").onclick = previewFile("imagem","file");



function inputImagem(){
	var formulario = document.querySelector("form")
	var qtdimagens = document.querySelectorAll(".noticia-imagem").length;
	var excluir = document.createElement("div");
		excluir.textContent = "X";
		excluir.onclick = removerPai;
		excluir.className = "btn btn-danger";


	var row_livre = document.createElement("div");
		row_livre.className = "row column";
		row_livre.setAttribute("draggable","true");
		row_livre.addEventListener('dragstart', handleDragStart, false);
		row_livre.addEventListener('dragenter', handleDragEnter, false);
		row_livre.addEventListener('dragover', handleDragOver, false);
		row_livre.addEventListener('dragleave', handleDragLeave, false);
		row_livre.addEventListener('drop', handleDrop, false);
		row_livre.addEventListener('dragend', handleDragEnd, false);

	var row_imagem = document.createElement("div");
		row_imagem.className = "row noticia-imagem";

	var col_sm = document.createElement("div")
		col_sm.className = "col col-sm";

	var row = document.createElement("div")

	var col_input = document.createElement("div")
		col_input.className = "col seguraInput";
	var input = document.createElement("input")
		input.className = "inputfile";
		input.id = "file-"+(qtdimagens+1);
		input.type= "file";
		input.addEventListener('change', previewFile);
		// input.onchange = previewFile;
		// input.setAttribute("onchange","previewFile");
	var label = document.createElement("label");
	 	label.textContent = "Escolha a imagem";
		label.setAttribute("for","file-"+(qtdimagens+1));

	var row_img = document.createElement("div");
		row_img.className = "row";
	var col_img = document.createElement("div");
		col_img.className = "col";
	var img = document.createElement("img");
		img.id = "imagem-file-"+(qtdimagens+1);
		img.src="";

	formulario.appendChild(row_livre);
		row_livre.appendChild(row_imagem);
			row_imagem.appendChild(col_sm);
				col_sm.appendChild(row);
					row.appendChild(col_input);
						col_input.appendChild(input);
						col_input.appendChild(label);
				col_sm.appendChild(row_img);
					row_img.appendChild(col_img);
						col_img.appendChild(img);
		row_livre.appendChild(excluir);
	return row_livre;
}

function campoTitulo(){
	var formulario = document.querySelector("form");
	var qtdtitulo = document.querySelectorAll(".noticia-titulo").length;
	var excluir = document.createElement("div");
		excluir.textContent = "X";
		excluir.onclick = removerPai;
		excluir.className = "btn btn-danger";


	var row_livre = document.createElement("div");
		row_livre.className = "row column";
		row_livre.setAttribute("draggable","true");
		row_livre.addEventListener('dragstart', handleDragStart, false);
		row_livre.addEventListener('dragenter', handleDragEnter, false);
		row_livre.addEventListener('dragover', handleDragOver, false);
		row_livre.addEventListener('dragleave', handleDragLeave, false);
		row_livre.addEventListener('drop', handleDrop, false);
		row_livre.addEventListener('dragend', handleDragEnd, false);

	var row = document.createElement("div");
		row.className = "row noticia-titulo";

	var h1 = document.createElement("h1");
	var span = document.createElement("span")
		span.textContent = "Titulo";
		span.setAttribute("contenteditable", "true");

	formulario.appendChild(row_livre);
		row_livre.appendChild(row);
			row.appendChild(h1);
				h1.appendChild(span);
		row_livre.appendChild(excluir);
	return row_livre;
}

function campoTexto(){
	var formulario = document.querySelector("form");
	var qtdtexto = document.querySelectorAll(".noticia-texto").length;
	var excluir = document.createElement("div");
		excluir.textContent = "X";
		excluir.onclick = removerPai;
		excluir.className = "btn btn-danger";

	
	var row_livre = document.createElement("div");
		row_livre.className = "row column";
		row_livre.setAttribute("draggable","true");
		row_livre.addEventListener('dragstart', handleDragStart, false);
		row_livre.addEventListener('dragenter', handleDragEnter, false);
		row_livre.addEventListener('dragover', handleDragOver, false);
		row_livre.addEventListener('dragleave', handleDragLeave, false);
		row_livre.addEventListener('drop', handleDrop, false);
		row_livre.addEventListener('dragend', handleDragEnd, false);

	// var row = document.createElement("div");
	// 	row.classList = "noticia-texto";
	var row = document.createElement("div");
		row.className = "row noticia-texto";
	var paragrafo = document.createElement("p");
		paragrafo.id = "paragrafo"+qtdtexto;
		paragrafo.className = "estiloP"
		paragrafo.textContent = "Texto aqui:" 
		paragrafo.setAttribute("contenteditable", "true");

	formulario.appendChild(row_livre);
		row_livre.appendChild(excluir);
		row_livre.appendChild(row);
		row.appendChild(paragrafo);
	return row_livre;
}

function removerPai(){
	// console.log(this.parentNode.remove());
	this.parentNode.remove();
}




var dragSrcEl = null;

function handleDragStart(e) {
  this.style.opacity = '0.4';  // this / e.target is the source node.
  dragSrcEl = this;

  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text/html', this.innerHTML);
}


function handleDragOver(e) {
	  // this.style.opacity = '1';  // this / e.target is the source node.

  if (e.preventDefault) {
    e.preventDefault(); // Necessary. Allows us to drop.
  }

  e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.

  return false;
}

function handleDragEnter(e) {
  // this / e.target is the current hover target.
  // adiciona a classe over que gera o efeito de linha tracejada 
  this.classList.add('over');
}
function handleDragLeave(e) {

  // this / e.target is previous target element.
  // remove a classe over que gera o efeito de linha tracejada 
  this.classList.remove('over');
}


function handleDrop(e) {
  // this/e.target is current target element.

  // if (e.stopPropagation) {
  //   e.stopPropagation(); // Stops some browsers from redirecting.
  // }

  // Don't do anything if dropping the same column we're dragging.
  if (dragSrcEl != this) {
    // Set the source column's HTML to the HTML of the columnwe dropped on.
    dragSrcEl.innerHTML = this.innerHTML;
    this.innerHTML = e.dataTransfer.getData('text/html');
	var inputs = document.querySelectorAll(".inputfile");
	inputs.forEach( function(element, index) {
		// statements
		element.addEventListener('change', previewFile);
	});
  }


  return false;
}

function handleDragEnd(e) {
  // this/e.target is the source node.
  // alert("soutou")
var cols = document.querySelectorAll('.noticias .column');
  [].forEach.call(cols, function (col) {
    col.classList.remove('over');
    col.style.opacity = '1';  // this / e.target is the source node.
  });
}


/*buscar noticias*/
 var btn_noticia_buscar = document.querySelector("#buscar-noticia");
 	btn_noticia_buscar.addEventListener("click", buscar_noticia);

 function buscar_noticia(){
 	var input = document.querySelector("#numero");
 	if (input.value>=0) {
 		ajax_buscar_noticia(input.value);
 	}
 }

function ajax_buscar_noticia(numero){
    // console.log(i)
    var xhr = new XMLHttpRequest();
  	xhr.onreadystatechange = function (){
      // esse callback é chamado várias vezes para cada mudança de readyState
      // readyState 4 é chamado quando o request é concluído (mesmo que .done() do jQuery)
      // você também pode checar o this.status para ver código de retorno HTTP
      // 200 é sucesso
      // 404 é página não encontrada
      // 403 é acesso negado
      // 500 é erro interno do servidor etc
      // if (this.readyState == 1)
      // {
      //     alert(this.responseText);
      // }
      // if (this.readyState == 2)
      // {
      //     alert(this.responseText);
      // }
      // if (this.readyState == 3)
      // {
      //     alert(this.responseText);
      // }
      if (this.readyState == 4)
      {
      	var data = this.responseText
  //     	console.log("data:")
  //     	console.log(data)
  //     	// console.log(data)
		if (this.status == 800) {
			document.querySelector(".noticias").innerHTML = "";
			alert("Cadastro não encontrado")
		}else{
			desenhar_noticia(JSON.parse(data));
		}
		// alert(this.status)




      	     		// statement
        // var div = document.querySelector(".alert-server");
        // var h1 = document.querySelector(".alert-server h1");

        // h1.textContent = "Buscar noticia";
        // h1.classList = "btn-success";

        // div.style.top = '0em';
        // setTimeout(function(){ 
        //   div.style.top = '-3em';
        //  }, 4000)      
        // setTimeout(function(){ 
        // h1.textContent = "";
        // h1.classList = "";
        //  }, 5000)          
      }
 //      	if (ajax.readyState == 4 && ajax.status == 200) {
    
	// 	var data = ajax.responseText;
		
 //    // Retorno do Ajax
	// 	console.log(data);
	// }
  }
    xhr.open("GET", "/painel/noticias/"+numero,true);

    xhr.setRequestHeader("Content-type", "application/json");
    console.log(numero)
    xhr.send();


}
function desenhar_noticia(noticia){
	console.log(noticia)
	document.querySelector(".noticias").innerHTML = "";
	document.querySelector(".noticias").innerHTML = "<input id='noticia-id' type='hidden' value='"+noticia._id+"''>";
	// var botao =  document.createElement("button")
	// 	botao.id = "alterar-noticia ";
	// 	botao.className = "btn btn-primary";
	// 	botao.textContent = "ALTERAR";
	// 	botao.onclick = alterarNoticia;

		// button#salvar-noticia.btn.btn-primary Salvar
	// document.querySelector(".row.save").appendChild(botao);

	

	noticia.linhas.forEach( function(element, index) {
		console.log(element.tipo)
		if(element.tipo == "imagem"){
			
			 integrar_inputImagem(inputImagem(),element.conteudo);
		}
		else if(element.tipo == "titulo"){
			
			 integrar_titulo(campoTitulo(),element.conteudo);
		}else{
			
			 integrar_texto(campoTexto(),element.conteudo);
		}
	});
}
function integrar_inputImagem(formulario,conteudo){
	formulario.querySelector("img").src = conteudo;
}
function integrar_titulo(formulario,conteudo){
	formulario.querySelector("span").textContent = conteudo;
}
function integrar_texto(formulario,conteudo){
	console.log(formulario)
	formulario.querySelector(".estiloP").textContent = conteudo;
}

// function alterarNoticia(){
	
// }