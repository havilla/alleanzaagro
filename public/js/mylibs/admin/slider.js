window.onload = initPage;
function initPage(){
  addExcluir();
  addDnD();
}
var but_imagem = document.querySelector("#file-1");
    but_imagem.addEventListener("change",moverImagem,false);

var but_salvar_imagem = document.querySelector("#salvar-slider");
    but_salvar_imagem.addEventListener("click", salvarImagens);

 function moverImagem(evt) {
    var files = evt.target.files;

    for (var i = 0, f; f = files[i]; i++) {

      if (!f.type.match('image.*')) {
        continue;
      } //verifica se os arquivos são imagens

      var reader = new FileReader();

      reader.onload = (function(filei) {
        return function(e) {

          var img = document.createElement("img");
              img.src = e.target.result;
              img.title = escape(filei.name);
              img.className = "d-block img-fluid";

          var excluir = document.createElement("div");
              excluir.textContent = "X";
              excluir.onclick = removerPai;
              excluir.className = "btn btn-danger";

          var input = document.createElement("input");
              input.className = "slider_input";


          var tag = document.createElement('div');
              tag.className = "carousel-item";
              DnD(tag);
              tag.appendChild(img);
              tag.appendChild(excluir);
              tag.appendChild(input);

          document.getElementById('resultados').insertBefore(tag, null);
          input.focus();
        };
      })(f);
      reader.readAsDataURL(f);
    }
  }
function removerPai(){
  this.parentNode.remove();
}
function salvarImagens(){
var imgs = document.querySelectorAll(".carousel-item");
  if (imgs.length > 4) {
        alertGlobal("Máximo 4 imagens!","","","FECHAR");
  } else {
    var imagens = [];
    imgs.forEach(function(element, index) {
      console.log(element)
      var objeto = {}
      objeto.img = element.querySelector("img").src;
      objeto.src = element.querySelector("img").title;
      objeto.texto = element.querySelector("input").value;
      imagens.push(objeto)
    });
    ajaxImagens(JSON.stringify(imagens));
  }
}

function ajaxImagens(i){
  $.ajax({
      type: 'POST',
      contentType: "application/json",
      url: "/slider",
      data: i,
      beforeSend: function(){
      },
      success: function(data, textStatus, jqXHR, Exception){
        alertGlobal("Salvo com Sucesso!","","","FECHAR");
      },
      error: function(jqXHR, textStatus){
        alertGlobal("Tente novamente!","" , "","FECHAR");
      }
    });


}

function addExcluir(){
  var btns = document.querySelectorAll(".btn-danger");
  btns.forEach( function(element, index) {
    element.onclick = removerPai;
  });

}

function addDnD(){
  var div = document.querySelectorAll("#resultados .carousel-item");
  div.forEach( function(element, index) {
    DnD(element)
  });

}

// arrastar e soutar


function DnD(alvo){
    // var row_livre = document.createElement("div");
    alvo.classList.add("column")
    alvo.setAttribute("draggable","true");
    alvo.addEventListener('dragstart', handleDragStart, false);
    alvo.addEventListener('dragenter', handleDragEnter, false);
    alvo.addEventListener('dragover', handleDragOver, false);
    alvo.addEventListener('dragleave', handleDragLeave, false);
    alvo.addEventListener('drop', handleDrop, false);
    alvo.addEventListener('dragend', handleDragEnd, false);
}

var dragSrcEl = null;

function handleDragStart(e) {
  this.style.opacity = '0.4';  // this / e.target is the source node.
  dragSrcEl = this;

  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text/html', this.innerHTML);
}

function handleDragOver(e) {
  if (e.preventDefault) {
    e.preventDefault(); // Necessary. Allows us to drop.
  }

  e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.

  return false;
}

function handleDragEnter(e) {
  this.classList.add('over');
}
function handleDragLeave(e) {
  this.classList.remove('over');
}


function handleDrop(e) {
  if (dragSrcEl != this) {
    dragSrcEl.innerHTML = this.innerHTML;
    this.innerHTML = e.dataTransfer.getData('text/html');
  var div = document.querySelectorAll("#resultados .carousel-item");
  div.forEach( function(element, index) {
    element.style.opacity = '1';
    element.classList.remove('over');

  });
  addExcluir();
  }
  return false;
}

function handleDragEnd(e) {
var cols = document.querySelectorAll('.noticias .column');
  [].forEach.call(cols, function (col) {
    col.classList.remove('over');
    col.style.opacity = '1';  // this / e.target is the source node.
  });
}




// var divInput = document.querySelector("#inputOrigem");
// var formImagem = document.querySelector(".galeria")
// eventoChange();

// function eventoChange() {
// 	divInput.querySelector("input").addEventListener("change", function() {
// 		// formImagem.appendChild(this);
// 		readImage(this);
// 		divInput.appendChild(criarInput())
// 		// eventoChange();
// 	})
// }
// function criarInput() {
// 	var input = document.createElement("input");
// 	input.name = "photos";
// 	input.type = "file";
// 	return input
// }
// function readImage(input) {
// 	if ( input.files && input.files[0] ) {
// 		var FR= new FileReader();
// 		FR.onload = function(e) {
// 			// var img = document.createElement("img");
// 			// img.src = e.target.result;
// 			$(".galeria").append(
// 				'<div class="imgUploadint">'+
// 					'<div class="imagemUpl">' +
// 						'<a class="expandir_IMG" href="' + e.target.result + '">'+
// 							'<img class="tmimgupl1" src="' + e.target.result + '" alt="imagem"/>'+
// 							'<input class="inputSlidPrinc" name="textSlider[]">'+
// 						'</a>'+
// 					'</div>' +         		    	
// 					// '<div class="div-input-falso cadSlide">'+
// 					// 	// '<input placeholder="Digite o texto"name="file-falso" type="text"  value=""/>'+
// 					// '</div>' + 
// 					'<a class="removeupload1">'+
// 						'<div class="closeUpl" >+</div>'+
// 					'</a>'+
// 				'</div>'
// 			);
// 			$(".imgUploadint:last-child").append(input);
// 			$(".removeupload1").bind('click',Excluir);
// 			eventoChange();
// 			// $('.expandir_IMG').off("click");
// 		};       
// 		FR.readAsDataURL( input.files[0] );
// 	}
// }

// var qtd = 0;
// function Excluir(){
//     var par = $(this).parent(); 	    
//     par.remove();
// 	$("#caminImg").val('');
//     qtd =  qtd - 1; 
// }
// removeImg()
// function removeImg(){
// 	$(".imgUploadint .removeupload1").each(function(index, el) {
// 		$(el).bind("click",Excluir)
// 	});
// }

// $(function(){
// 	$('#formImagem').submit(function(event) {
// 		if($('.galeria img').length>4){
// 			alertGlobal("Número Máximo de Imagens Permitidas: 4","" , "","FECHAR");
// 			return false;
// 		}
		
// 	});
// })