// $(function(){
// 	//Mover Menu suave
//   var $doc = $('html, body');
//   $('a.scroll').click(function() {
//       $doc.animate({
//           scrollTop: $( $.attr(this, 'href') ).offset().top
//       }, 2000);
//       return false;
//   });
// });

$(function() {
  //Mover Menu suave
  $('a.nav-link, a.logo-esq, a.linkfooter').click(function(event) {
    moverSuave($("#" + $(this).attr('name') + "").offset().top);
  });

  function moverSuave(param) {
    $("body,html").animate({
      scrollTop: param
    }, 2000);
  }
})

//Subir Menu
// var posicaoInicial = $('#empresa').position().top;
  $(document).scroll(function() {
    var posicaoScroll = $(document).scrollTop();
    if (posicaoScroll > 0) {
      $('ul.nav.navbar-nav.navbar-left').addClass('rigth0');
      // $('ul.nav.navbar-nav.navbar-rigth').addClass('left0');
      $('#logomarca').addClass('esconder');
      $('#logomarcaA').addClass('aparecer');
      // $('#logomarcahorz').addClass('aparecer');
    } else {
      $('ul.nav.navbar-nav.navbar-left').removeClass('rigth0');
      // $('ul.nav.navbar-nav.navbar-rigth').removeClass('left0');
      $('#logomarca').removeClass('esconder')
      $('#logomarcaA').removeClass('aparecer');
      // $('#llogomarcahorz').removeClass('aparecer');
    }
  })

//PARALLAX
$(document).ready(function(){
 $window = $(window);
 $('section[data-type="background"]').each(function(){
   var $scroll = $(this);                 
    $(window).scroll(function() {
      var yPos = -($window.scrollTop() / $scroll.data('speed')); 
       var coords = '50% '+ yPos + 'px';
      $scroll.css({ backgroundPosition: coords });    
    });
 });  
}); 

$('.nav-link, a.logo-esq').click(function() {
  $('.nav-link.ativo').removeClass('ativo')
  $(this).addClass('ativo');
})