var url = require('url');

module.exports = function(req,res){
	var createUrl = url.parse(req.url).pathname == "/usuario/create";
	var updateUrl = !createUrl;

	req.assert('nome','Informe o seu Nome').notEmpty();
	if (createUrl) {
		req.assert('email', "E-mail inválido").isEmail();
		req.assert('password', 'Sua senha deve conter no minimo 6 caracter e no maximo 12.').len(6,10);
	}
	// req.assert('site', 'Site não e uma url válida').isURL();

	var validateErros = req.validationErrors() || [];

	if(req.body.password != req.body.password_confirmar){
		validateErros.push({msg: 'Senha não confere.'})
	}
	if(validateErros.length > 0){
		validateErros.forEach(function(e){
			req.flash('erro', e.msg);
		})
		return false;
	}else{
		return true;
	}
}