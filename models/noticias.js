var mongoose = require('mongoose');
module.exports = function() {
	var linhasSchema = mongoose.Schema({
		tipo	: {type: String},
		conteudo	: {type: String, trim: true}
	})
	var noticiaSchema = mongoose.Schema({
		estado: {type: Boolean},
		numero: {type: String, require: true, trim: true, unique: true },
		linhas: [linhasSchema],
		data_cad: {type: Date,	default: Date.now}
	})
	return mongoose.model('Noticia', noticiaSchema)

}