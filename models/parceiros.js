var mongoose = require('mongoose');
module.exports = function() {

	var parceirosSchema = mongoose.Schema({
		nome: {type: String},
		telefone: {type: String},
		uf: {type: String},
		cidade: {type: String},
		image: {type: String},
		data_cad: {type: Date,	default: Date.now}
	})
	return mongoose.model('Parceiros', parceirosSchema)

}